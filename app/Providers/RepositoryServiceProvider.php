<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\CommentRepository::class,
            \App\Repositories\CommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NewsRepository::class,
            \App\Repositories\NewsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SubscriberRepository::class,
            \App\Repositories\SubscriberRepositoryEloquent::class);
        //:end-bindings:
    }
}
