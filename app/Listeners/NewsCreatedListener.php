<?php

namespace App\Listeners;

use App\Entities\User;
use App\Notifications\NewsCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
//        dd($event->news);
        foreach($event->news->user->subscribers as $subscriber) {
            $subscriber->notify(new NewsCreated($event->news));
        }
    }
}
