<?php

namespace App\Http\Controllers;

use App\Events\NewsCreated;
use App\Http\Requests\NewsStoreRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Http\Resources\NewsResource;
use App\Repositories\NewsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;

class NewsController extends Controller
{

    /**
     * @var NewsRepository
     */
    protected $news;

    /**
     * NewsController constructor.
     * @param NewsRepository $news
     */
    public function __construct(NewsRepository $news)
    {
        $this->news = $news;
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if($request->has('sort') && $request->sort == 'comment') {
            $result = $this->news->withCount('comments')
                ->orderBy('comments_count', 'desc')->all();
            if($request->has('limit')) {
                $result = $result->take($request->limit);
            }
        }
        else $result = $this->news->all();

        return NewsResource::collection($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsStoreRequest $request)
    {
        $news = Auth::user()->news()->create([
            'title' => $request->title,
            'description' => $request->description
        ]);
        if($news) {
            event(new NewsCreated($news));
        }
        return new NewsResource($news);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repositories\NewsRepository  $newsRepository
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new NewsResource($this->news->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repositories\NewsRepository  $newsRepository
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repositories\NewsRepository  $newsRepository
     * @return \Illuminate\Http\Response
     */
    public function update(NewsUpdateRequest $request, $id)
    {
        Validator::make(['id' => $id, 'title' => $request->title, 'description' => $request->description], [
            'id' =>'required|exists:news,id',
            'title' => [
                'required',
                Rule::unique('news')->ignore($id),
            ],
            'description' => 'required'
        ])->validate();
        $data = $request->only(['title', 'description']);
        return new NewsResource($this->news->update($data, $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repositories\NewsRepository  $newsRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsUpdateRequest $request, $id)
    {
        Validator::make(['id' => $id], [
            'id' => [
                'required',
                'exists:news,id',
            ]
        ])->validate();
        $this->news->delete($id);

        return response()->json('News ' . $id . ' was successfully deleted!');
    }
}
