<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentDestroyRequest;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpdateRequest;
use App\Http\Resources\CommentResource;
use App\Repositories\CommentRepository;
use App\Repositories\NewsRepository;
use Validator;

class CommentController extends Controller
{

    /**
     * @var CommentRepository
     */
    protected $comment;

    /**
     * @var NewsRepository
     */
    protected $news;

    /**
     * CommentController constructor.
     * @param CommentRepository $comment
     * @param NewsRepository $news
     */
    public function __construct(CommentRepository $comment, NewsRepository $news)
    {
        $this->comment = $comment;
        $this->news = $news;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request)
    {
        return new CommentResource($this->comment->comment($this->news, $request->only('title', 'body'), $request->news_id, $request->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentUpdateRequest $request, $id)
    {
       return new CommentResource($this->comment->update($request->only('title', 'body'), $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentDestroyRequest $request, $id)
    {
        $this->comment->delete($id);
        return response()->json([
            'message' => 'Comment was successfully deleted!'
        ]);
    }
}
