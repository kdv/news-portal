<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\ConfirmedUserException;
use App\Exceptions\NotValidConfirmTokenException;
use App\Http\Requests\RegisterConfirmRequest;
use App\Repositories\UserRepositoryEloquent;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));
        $message = [
            'message' => 'User successfully register. Check your email for confirmation.'
        ];

        return response()->json($message);
    }

    /**
     * @param RegisterConfirmRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ConfirmedUserException
     * @throws NotValidConfirmTokenException
     */
    public function confirm(RegisterConfirmRequest $request)
    {
        $userRepository = App::make(UserRepositoryEloquent::class);
        $user = $userRepository->findByField('email', $request->email)->first();
        if($user->confirmed) {
            throw new ConfirmedUserException();
        }
        if(Hash::check($user->created_at, $request->token)) {
            $userRepository->confirm($request->email);

            return response()->json('Confirmed successfully!');
        }
        else throw new NotValidConfirmTokenException();
    }

}
