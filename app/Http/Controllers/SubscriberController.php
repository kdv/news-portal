<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriberDestroyRequest;
use App\Http\Requests\SubscriberStoreRequest;
use App\Repositories\SubscriberRepository;

class SubscriberController extends Controller
{
    /**
     * @var SubscriberRepository
     */
    protected $subscriber;

    /**
     * SubscriberController constructor.
     * @param SubscriberRepository $subscriber
     */
    public function __construct(SubscriberRepository $subscriber)
    {
        $this->subscriber = $subscriber;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriberStoreRequest $request)
    {
        $subscriber = $this->subscriber->subscribe($request->email, $request->user_id);
        dd($subscriber);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubscriberDestroyRequest $request, $user_id)
    {
        $this->subscriber->unsubscribe($request->email, $user_id);
    }
}
