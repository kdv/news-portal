<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepositoryEloquent;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    /**
     * @return mixed
     */
    public function show()
    {
        return App::make(UserTransformer::class)->transform(Auth::user());
    }
}
