<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => (int) $this->id,

            'username' => $this->username,
            'email' => $this->email,
            'countNews' => $this->news()->count(),
            'countSubscribers' => $this->subscribers()->count(),

            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
