<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class NewsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => (int) $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'comments' => CommentResource::collection($this->comments()->where('parent_id', null)->get()),
            'countComments' => $this->commentCount(),
            'author' => $this->user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
