<?php

namespace App\Http\Middleware;

use App\Exceptions\NotConfirmedUserException;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserConfirmation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->confirmed) {
            throw new NotConfirmedUserException();
        }
        return $next($request);
    }
}
