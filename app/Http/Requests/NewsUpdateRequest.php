<?php

namespace App\Http\Requests;

use App\Repositories\NewsRepositoryEloquent;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class NewsUpdateRequest extends FormRequest
{
    protected $urlParameters = [
        'news',
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::id() == App::make(NewsRepositoryEloquent::class)->find($this->route('news'))->user_id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

        ];
    }
}
