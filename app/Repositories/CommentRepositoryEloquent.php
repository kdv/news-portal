<?php

namespace App\Repositories;

use App\Entities\News;
use App\Exceptions\CommentNotCommentable;
use BrianFaust\Commentable\Models\Comment;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CommentRepository;

/**
 * Class CommentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CommentRepositoryEloquent extends BaseRepository implements CommentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Comment::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $newsRepository
     * @param $data
     * @param $id
     * @param null $parent_id
     * @return mixed
     * @throws CommentNotCommentable
     */
    public function comment($newsRepository, $data, $id, $parent_id = null)
    {
        if($parent_id) {
            if($this->find($parent_id)->parent_id) {
                throw new CommentNotCommentable();
            }
            else {
                $news = $newsRepository->find($id);
                return $news->comment($data, Auth::user(), $this->find($parent_id));
            }
        }
        else {
            $news = $newsRepository->find($id);
            return $news->comment($data, Auth::user());
        }
    }
}
