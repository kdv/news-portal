<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SubscriberRepository;
use App\Entities\Subscriber;
use App\Validators\SubscriberValidator;

/**
 * Class SubscriberRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SubscriberRepositoryEloquent extends BaseRepository implements SubscriberRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Subscriber::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function subscribe($email, $user_id)
    {
        $subscriber = $this->firstOrCreate(['email' => $email]);
        if(!$subscriber->users()->where('user_id', $user_id)->first()) {
            $subscriber->users()->attach($user_id);
        }

        return $subscriber->users;
    }

    public function unsubscribe($email, $user_id)
    {
        $subscriber = $this->firstOrCreate(['email' => $email])->users()->detach($user_id);

        return $subscriber;
    }
}
