<?php

namespace App\Exceptions;

use Exception;

class CommentNotCommentable extends Exception
{
    protected $message = 'This comment is not commentable!';
}
