<?php

namespace App\Exceptions;

use Exception;

class ConfirmedUserException extends Exception
{
    protected $message = 'User already has been confirmed!';
}
