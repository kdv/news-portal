<?php

namespace App\Exceptions;

use Exception;

class NotConfirmedUserException extends Exception
{
    protected $message = 'User not confirmed, check your email for confirmation!';
}
