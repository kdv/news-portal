<?php

namespace App\Exceptions;

use Exception;

class NotValidConfirmTokenException extends Exception
{
    protected $message = 'Invalid confirmation token!';
}
