<?php

namespace App\Entities;

use App\Traits\HasComments;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class News
 * @package App\Entities
 */
class News extends Model implements Transformable
{
    use TransformableTrait, HasComments;

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'user_id',
    ];

    /**
     * Creator of entity
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(User::class,  'user_id', 'id');
    }

}
