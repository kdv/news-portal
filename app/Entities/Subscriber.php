<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;

class Subscriber extends Model implements Transformable
{
    use TransformableTrait, Notifiable;

    protected $fillable = [
        'email',
        'user_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_subscriber', 'subscriber_id', 'user_id');

    }

}
