<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('news/most-commentable');

Route::resource('/news', 'NewsController',
    ['only' => [
        'index', 'show'
    ]
    ]);

Route::middleware(['guest'])->group(function(){
    Route::post('/register', [
        'uses' => 'Auth\RegisterController@register',
    ]);

    Route::get('/confirm', [
        'uses' => 'Auth\RegisterController@confirm',
    ]);
});



Route::middleware(['auth:api', 'confirmed'])->group(function(){

    Route::get('/user', [
        'uses' => 'UserController@show'
    ]);

    Route::resource('/news', 'NewsController',
        ['except' => [
            'create', 'edit', 'index', 'show'
        ]
    ]);

    Route::resource('/subscribers', 'SubscriberController',
        [
            'only' => ['store', 'destroy']
        ]);

    Route::resource('/comments', 'CommentController');
});
